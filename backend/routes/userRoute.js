import express from "express";
import { getArtists, getArtistById, createArtist, updateArtist, deleteArtist } from "../controllers/userController.js";

const router = express.Router();

router.get("/artists", getArtists);
router.get("/artists/:id", getArtistById);
router.post("/artists", createArtist);
router.patch("/artists/:id", updateArtist);
router.delete("/artists/:id", deleteArtist);

export default router;
