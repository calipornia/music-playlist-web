import { Sequelize } from "sequelize";

const db = new Sequelize("simee_playlist", "root", "", {
	host: "localhost",
	dialect: "mysql",
});

export default db;
