import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Artist = db.define(
	"artists",
	{
		name: DataTypes.STRING,
		title: DataTypes.STRING,
		genre: DataTypes.STRING,
	},
	{
		freezeTableName: true,
	}
);

export default Artist;

(async () => {
	await db.sync();
})();
