import Artist from "../models/userModel.js";

export const getArtists = async (req, res) => {
	try {
		const response = await Artist.findAll();
		res.status(200).json(response);
	} catch (error) {
		console.log(error.message);
	}
};

export const getArtistById = async (req, res) => {
	try {
		const response = await Artist.findOne({
			where: {
				id: req.params.id,
			},
		});
		res.status(200).json(response);
	} catch (error) {
		console.log(error.message);
	}
};

export const createArtist = async (req, res) => {
	try {
		await Artist.create(req.body);
		res.status(201).json({ msg: "Artis ditambahkan!" });
	} catch (error) {
		console.log(error.message);
	}
};

export const updateArtist = async (req, res) => {
	try {
		await Artist.update(req.body, {
			where: {
				id: req.params.id,
			},
		});
		res.status(200).json({ msg: "Artis berhasil diubah!" });
	} catch (error) {
		console.log(error.message);
	}
};

export const deleteArtist = async (req, res) => {
	try {
		await Artist.destroy({
			where: {
				id: req.params.id,
			},
		});
		res.status(200).json({ msg: "Artis dihapuskan!" });
	} catch (error) {
		console.log(error.message);
	}
};
