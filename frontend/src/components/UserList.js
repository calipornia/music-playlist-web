import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const UserList = () => {
	const [artists, setArtist] = useState([]);

	useEffect(() => {
		getArtists();
	}, []);

	const getArtists = async () => {
		const response = await axios.get("http://localhost:1205/artists");
		setArtist(response.data);
	};

	const deleteUser = async (id) => {
		try {
			await axios.delete(`http://localhost:1205/artists/${id}`);
			getArtists();
		} catch (error) {
			console.log("error maszeh");
		}
	};

	return (
		<div className="columns mt-5 is-centered">
			<div className="column is-half">
				<Link to={`/add`} className="button is-success">
					Add new
				</Link>
				<table className="table is-striped is-fullwidth">
					<thead>
						<tr>
							<th>No</th>
							<th>Artist</th>
							<th>Title</th>
							<th>Genre</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{artists.map((artist, index) => (
							<tr key={artist.id}>
								<td>{index + 1}</td>
								<td>{artist.name}</td>
								<td>{artist.title}</td>
								<td>{artist.genre}</td>
								<td>
									<Link to={`edit/${artist.id}`} className="button is-small is-info">
										Edit
									</Link>
									<button onClick={() => deleteUser(artist.id)} className="button is-small is-danger">
										Delete
									</button>
								</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		</div>
	);
};

export default UserList;
