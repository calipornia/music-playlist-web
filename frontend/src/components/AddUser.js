import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const AddUser = () => {
	const [name, setName] = useState("");
	const [title, setTitle] = useState("");
	const [genre, setGenre] = useState("Punk");
	const navigate = useNavigate();

	const saveUser = async (e) => {
		e.preventDefault();
		try {
			await axios.post("http://localhost:1205/artists", {
				name,
				title,
				genre,
			});
			navigate("/");
		} catch (error) {
			console.log("eror maszeh");
		}
	};

	return (
		<div className="columns mt-5 is-centered">
			<div className="column is-half">
				<form onSubmit={saveUser}>
					<div className="field">
						<label className="label">Name</label>
						<div className="control">
							<input type="text" className="input" value={name} onChange={(e) => setName(e.target.value)} placeholder="Masukan nama..." />
						</div>
					</div>
					<div className="field">
						<label className="label">Title</label>
						<div className="control">
							<input type="text" className="input" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Masukan Title..." />
						</div>
					</div>
					<div className="field">
						<label className="label">Genre</label>
						<div className="control">
							<div className="select is-fullwidth">
								<select value={genre} onChange={(e) => setGenre(e.target.value)}>
									<option value="Punk">Punk</option>
									<option value="Metal">Metal</option>
									<option value="Alternative">Alternative</option>
									<option value="Rock">Rock</option>
									<option value="pop">pop</option>
									<option value="Indie">Indie</option>
									<option value="Psychedelic">Psychedelic</option>
									<option value="Shoegaze">Shoegaze</option>
								</select>
							</div>
						</div>
					</div>
					<div className="field">
						<button type="submit" className="button is-success">
							Save
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default AddUser;
