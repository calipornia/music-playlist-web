import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

const EditUser = () => {
	const [name, setName] = useState("");
	const [title, setTitle] = useState("");
	const [genre, setGenre] = useState("Punk");
	const navigate = useNavigate();
	const { id } = useParams();

	useEffect(() => {
		getArtistById();
	}, []);

	const updateUser = async (e) => {
		e.preventDefault();
		try {
			await axios.patch(`http://localhost:1205/artists/${id}`, {
				name,
				title,
				genre,
			});
			navigate("/");
		} catch (error) {
			console.log("eror maszeh");
		}
	};

	const getArtistById = async () => {
		const response = await axios.get(`http://localhost:1205/artists/${id}`);
		setName(response.data.name);
		setTitle(response.data.title);
		setGenre(response.data.genre);
	};

	return (
		<div className="columns mt-5 is-centered">
			<div className="column is-half">
				<form onSubmit={updateUser}>
					<div className="field">
						<label className="label">Name</label>
						<div className="control">
							<input type="text" className="input" value={name} onChange={(e) => setName(e.target.value)} placeholder="Masukan nama..." />
						</div>
					</div>
					<div className="field">
						<label className="label">Title</label>
						<div className="control">
							<input type="text" className="input" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Masukan Title..." />
						</div>
					</div>
					<div className="field">
						<label className="label">Genre</label>
						<div className="control">
							<div className="select is-fullwidth">
								<select value={genre} onChange={(e) => setGenre(e.target.value)}>
									<option value="Punk">Punk</option>
									<option value="Metal">Metal</option>
									<option value="Alternative">Alternative</option>
									<option value="Rock">Rock</option>
									<option value="pop">pop</option>
									<option value="Indie">Indie</option>
									<option value="Psychedelic">Psychedelic</option>
									<option value="Shoegaze">Shoegaze</option>
								</select>
							</div>
						</div>
					</div>
					<div className="field">
						<button type="submit" className="button is-success">
							Update
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default EditUser;
